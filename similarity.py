# a="ADMIN ASST         ,Advisor            ,AGENT FROM VESTA   ,apouse             ,beau Fils          ,Beneficial Owner   ,Child/Son          ,childs father      ,co-owner           ,conjoin            ,Conjoint epoux     ,Conjugal Partner   ,DAUGHTER EXECUTOR  ,DAUGHTER, OTHER    ,Director           ,DRAND DAUGHT       ,ex spouse          ,fahtner            ,Father/Son         ,FatherHusband      ,Femme              ,Filles             ,filleuil           ,Foster Child       ,Foster Mother      ,GRANDPARENTS       ,great grandchildrren,Green party candidat,kims CIL partner   ,NEFEW              ,partener           ,petit-fils         ,REALESTATE AGENT   ,RECIPIENT OF PROCEED,Renter             ,Résident           ,SELF PC CANDIDATE  ,SENIOR OFFICE COORDI,she is my mother   ,Siblings           ,Sister inlaw       ,Son - Disabled     ,SON OF SPOUSE COUSIN,Son who is executor,spose              ,Spouse of Client   ,Team Coach         ,testament          ,Wife of Louis    "
# b =a.split(",")
# words=[]
# for k in b :
#     c=k.strip()
#     words.append(c)
# Author : Oulaidi Mhammed
from gensim.models import Word2Vec
 
from nltk.cluster import KMeansClusterer
import nltk
 
 
from sklearn import cluster
from sklearn import metrics
 
# training data
 
sentences = [['ADMIN ASST', 'Advisor', 'AGENT FROM VESTA', 'apouse', 'beau Fils', 'Beneficial Owner', 'Child/Son', 'childs father', 'co-owner', 'conjoin', 'Conjoint epoux', 'Conjugal Partner', 'DAUGHTER EXECUTOR', 'DAUGHTER', 'OTHER', 'Director', 'DRAND DAUGHT', 'ex spouse', 'fahtner', 'Father/Son', 'FatherHusband', 'Femme', 'Filles', 'filleuil', 'Foster Child', 'Foster Mother', 'GRANDPARENTS', 'great grandchildrren', 'Green party candidat', 'kims CIL partner', 'NEFEW', 'partener', 'petit-fils', 'REALESTATE AGENT', 'RECIPIENT OF PROCEED', 'Renter', 'R\xc3\xa9sident', 'SELF PC CANDIDATE', 'SENIOR OFFICE COORDI', 'she is my mother', 'Siblings', 'Sister inlaw', 'Son - Disabled', 'SON OF SPOUSE COUSIN', 'Son who is executor', 'spose', 'Spouse of Client', 'Team Coach', 'testament', 'Wife of Louis']]
 
# training model
model = Word2Vec(sentences, min_count=1)
 
# get vector data
X = model[model.wv.vocab]
print (X)
 
print (model.similarity('advisor', 'director'))
 
print (model.similarity('father', 'son'))
 
print (model.most_similar(positive=['beneficial owner'], negative=[], topn=2))
 
print (model['director'])
 
print (list(model.wv.vocab))
 
print (len(list(model.wv.vocab)))
 
 
 
 
NUM_CLUSTERS=3
kclusterer = KMeansClusterer(NUM_CLUSTERS, distance=nltk.cluster.util.cosine_distance, repeats=25)
assigned_clusters = kclusterer.cluster(X, assign_clusters=True)
print (assigned_clusters)
 
words = list(model.wv.vocab)
for i, word in enumerate(words):  
    print (word + ":" + str(assigned_clusters[i]))
 
 
 
kmeans = cluster.KMeans(n_clusters=NUM_CLUSTERS)
kmeans.fit(X)
 
labels = kmeans.labels_
centroids = kmeans.cluster_centers_
 
print ("Cluster id labels for inputted data")
print (labels)
print ("Centroids data")
print (centroids)
 
print ("Score (Opposite of the value of X on the K-means objective which is Sum of distances of samples to their closest cluster center):")
print (kmeans.score(X))
 
silhouette_score = metrics.silhouette_score(X, labels, metric='euclidean')
 
print ("Silhouette_score: ")
print (silhouette_score)
